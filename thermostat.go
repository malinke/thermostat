// Copyright 2019 Max Linke
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package main

import (
	"bytes"
	"context"
	"encoding/binary"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/armon/circbuf"
	"github.com/d2r2/go-dht"
	"github.com/julienschmidt/httprouter"
	"github.com/rs/cors"
)

type Measurement struct {
	Temperature float32
	Humidity    float32
	Timestamp   time.Time
}

type MeasurementStore struct {
	Temperature float32
	Humidity    float32
	Timestamp   int64
}

var Last = Measurement{Temperature: 23, Humidity: 60, Timestamp: time.Now()}
var NBYTES = binary.Size(MeasurementStore{})
var SENSOR = dht.DHT22
var PIN = 4

const NMAX = 60 * 24

var INTERVAL = time.Second

var Buffer *circbuf.Buffer
var BYTEORDER = binary.LittleEndian

// Take a measurement. Will be replaced with DHT22 thermostat probing code
func measure() (Measurement, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()
	temperature, humidity, _, err :=
		dht.ReadDHTxxWithContextAndRetry(ctx, SENSOR, PIN, true, 10)
	if err != nil {
		return Measurement{-300, -1, time.Now()}, err
	}
	return Measurement{temperature, humidity, time.Now()}, nil
}

// update circular buffer.
func writeCircBuffer(m Measurement) {
	buf := new(bytes.Buffer)
	store := MeasurementStore{Temperature: m.Temperature, Humidity: m.Humidity, Timestamp: m.Timestamp.Unix()}
	if err := binary.Write(buf, BYTEORDER, store); err != nil {
		fmt.Println("Encoding Measurement Failed:", err)
	}
	_, err := Buffer.Write(buf.Bytes())
	if err != nil {
		fmt.Println("Circular Buffer Could not be updated:", err)
	}
	Last = m
}

func min(x, y int) int {
	if x < y {
		return x
	}
	return y
}

func readCircBuffer() ([]Measurement, error) {
	reader := bytes.NewReader(Buffer.Bytes())
	written := Buffer.TotalWritten() / int64(NBYTES)
	elems := min(NMAX, int(written))
	ms := make([]Measurement, elems)
	for i := 0; i < elems; i++ {
		var stored MeasurementStore
		if err := binary.Read(reader, BYTEORDER, &stored); err != nil {
			return ms[:i], errors.New("Could not read from circbuffer")
		}
		ms[i].Temperature = stored.Temperature
		ms[i].Humidity = stored.Humidity
		ms[i].Timestamp = time.Unix(stored.Timestamp, 0)
	}
	return ms, nil
}

// continously measure at given interval
func measureInInterval(interval time.Duration) {
	for _ = range time.Tick(interval) {
		m, err := measure()
		if err != nil {
			log.Output(1, "Could not read from device")
		} else {
			writeCircBuffer(m)
		}
	}
}

func CurrentTemp(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	j, err := json.Marshal(Last)
	if err != nil {
		w.WriteHeader(501)
		fmt.Fprint(w, "[]")
		return
	}
	w.WriteHeader(200)
	fmt.Fprintf(w, "%s", j)
}

func TempHistory(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	ms, err := readCircBuffer()
	if err != nil {
		w.WriteHeader(501)
		fmt.Fprint(w, "[]")
		return
	}
	// convert to json
	j, err := json.Marshal(ms)
	if err != nil {
		w.WriteHeader(501)
		fmt.Fprint(w, "[]")
		return
	}
	w.WriteHeader(200)
	fmt.Fprintf(w, "%s", j)

}

func main() {
	fmt.Println("hello world")

	// Initialize Circular Buffer
	var err error
	Buffer, err = circbuf.NewBuffer(int64(NBYTES * NMAX))
	if err != nil {
		fmt.Println("Could not create buffer", err)
	}

	// Start measurements in long-lived go routine that updates a global variable
	m, err := measure()
	if err != nil {
		log.Fatal(m)
	}
	writeCircBuffer(m)
	go measureInInterval(time.Minute)

	// start http router
	router := httprouter.New()
	router.GET("/current", CurrentTemp)
	router.GET("/history", TempHistory)
	handler := cors.Default().Handler(router)
	log.Fatal(http.ListenAndServe(":8666", handler))
}
