# Thermostat

I have a RaspberryPi-2 with a temperature sensor in my home. I'm using the [go-dht](https://github.com/d2r2/go-dht) library to build a small REST API 

This ia a project I do for fun and learning. Use at your own risk!

## Examples

In the examples folder is a website that shows how the REST server can be used to have a UI for the thermostat. You can open the website on a browser or start a small http server to serve it, for example using `python3 -m http.server` within the examples folder.

<img src="example.png" alt="Example UI" width="200"/>

## Building and Running the Service

    go build
    ./thermostat

## Usage

Run the resulting binary on the raspberry pi directly. Currently the sensor type, DHT22, and GPIO pin, 4, are hardcoded. Those need to be changed if you have a different sensor or use a different pin. To read the current temperature and humidity you can use curl in a different shell:

    curl localhost:8666/current
